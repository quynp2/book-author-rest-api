package com.devcamp.authorbookrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthorBookRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthorBookRestApiApplication.class, args);
	}

}
