package com.devcamp.authorbookrestapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.authorbookrestapi.model.Author;
import com.devcamp.authorbookrestapi.service.AuthorService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AuthorController {
    @Autowired
    private AuthorService authorService;

    @GetMapping("/author-info")
    public Author getAuthorInfo(@RequestParam(required = true, name = "email") String Email) {
        ArrayList<Author> getAllAuthor = authorService.getAllAuthor();
        Author findEmailAuthor = new Author();
        for (Author authorElement : getAllAuthor) {
            if (authorElement.getEmail().equals(Email)) {
                findEmailAuthor = authorElement;
            }

        }
        return findEmailAuthor;

    }

    @GetMapping("/author-gender")
    public ArrayList<Author> getAuthorGender(@RequestParam(required = true, name = "gender") Character Gender) {
        ArrayList<Author> getAllAuthor = authorService.getAllAuthor();
        ArrayList<Author> GenderArrayFound = new ArrayList<>();
        Author findGenderAuthor = new Author();
        for (Author authorElement : getAllAuthor) {
            if (authorElement.getGender() == Gender) {
                findGenderAuthor = authorElement;
                GenderArrayFound.add(findGenderAuthor);

            }

        }
        return GenderArrayFound;

    }

}
