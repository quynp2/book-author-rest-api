package com.devcamp.authorbookrestapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.authorbookrestapi.model.Book;
import com.devcamp.authorbookrestapi.service.BookService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class BookController {
    @Autowired
    private BookService bookService;

    @GetMapping("/books")
    public ArrayList<Book> getAllBookK() {
        ArrayList<Book> allBook = bookService.getAllBook();
        return allBook;

    }

    @GetMapping("/book-quantity")
    public ArrayList<Book> getBookQuantity(@RequestParam(required = true, name = "quantityNumber") int qtyNumber) {
        ArrayList<Book> getAllBook = bookService.getAllBook();
        ArrayList<Book> getArrayBookFinded = new ArrayList<>();
        Book findBook = new Book();
        for (Book bookElement : getAllBook) {
            if (bookElement.getQty() >= qtyNumber) {
                findBook = bookElement;
                getArrayBookFinded.add(findBook);
            }

        }
        return getArrayBookFinded;
    }

    @GetMapping("/books/{id}")
    public Book getBookByIndex(@PathVariable(required = true) int id) {
        ArrayList<Book> allBook = bookService.getAllBook();
        Book findBook = new Book();

        if (id < allBook.size()) {
            findBook = allBook.get(id);

        }
        return findBook;

    }

}
