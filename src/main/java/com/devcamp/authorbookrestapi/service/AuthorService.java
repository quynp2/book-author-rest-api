package com.devcamp.authorbookrestapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.authorbookrestapi.model.Author;

@Service
public class AuthorService {

    Author NguyenVanA = new Author("Nguyen Van A", "nva@gmail.com", 'm');
    Author NguyenVanB = new Author("Nguyen Van Anh", "Nguyen@gmail.com", 'f');
    Author NguyenVanC = new Author("Nguyen Van Binh", "Van@gmail.com", 'm');
    Author NguyenVanD = new Author("Nguyen Van Danh", "na@gmail.com", 'f');
    Author NguyenVanE = new Author("Nguyen Van Em", "a@gmail.com", 'm');
    Author NguyenVanF = new Author("Nguyen Van France", "France@gmail.com", 'f');
    Author NguyenVanG = new Author("Nguyen Van Ga", "nvsssa@gmail.com", 'f');
    Author NguyenVanH = new Author("Nguyen Van Ha", "Ha@gmail.com", 'm');
    Author NguyenVanI = new Author("Nguyen Van Ich", "VanIch@gmail.com", 'f');
    Author NguyenVanK = new Author("Nguyen Van kICH", "kIch@gmail.com", 'f');
    Author NguyenVanL = new Author("Nguyen Van liet", "liet@gmail.com", 'm');
    Author NguyenVanM = new Author("Nguyen Van Mit", "Mith@gmail.com", 'f');

    public ArrayList<Author> getAuthorOfBook1() {
        ArrayList<Author> AuthorOfBook01 = new ArrayList<>();
        AuthorOfBook01.add(NguyenVanA);
        AuthorOfBook01.add(NguyenVanB);
        AuthorOfBook01.add(NguyenVanC);
        return AuthorOfBook01;

    }

    public ArrayList<Author> getAuthorOfBook2() {
        ArrayList<Author> AuthorOfBook02 = new ArrayList<>();
        AuthorOfBook02.add(NguyenVanD);
        AuthorOfBook02.add(NguyenVanE);
        AuthorOfBook02.add(NguyenVanF);
        return AuthorOfBook02;

    }

    public ArrayList<Author> getAuthorOfBook3() {
        ArrayList<Author> AuthorOfBook03 = new ArrayList<>();
        AuthorOfBook03.add(NguyenVanG);
        AuthorOfBook03.add(NguyenVanH);
        AuthorOfBook03.add(NguyenVanI);
        return AuthorOfBook03;

    }

    public ArrayList<Author> getAuthorOfBook4() {
        ArrayList<Author> AuthorOfBook04 = new ArrayList<>();
        AuthorOfBook04.add(NguyenVanK);
        AuthorOfBook04.add(NguyenVanL);
        AuthorOfBook04.add(NguyenVanM);
        return AuthorOfBook04;

    }

    public ArrayList<Author> getAllAuthor() {
        ArrayList<Author> allAuthor = new ArrayList<>();
        allAuthor.add(NguyenVanA);
        allAuthor.add(NguyenVanB);
        allAuthor.add(NguyenVanC);
        allAuthor.add(NguyenVanD);
        allAuthor.add(NguyenVanE);
        allAuthor.add(NguyenVanF);
        allAuthor.add(NguyenVanG);
        allAuthor.add(NguyenVanH);
        allAuthor.add(NguyenVanI);
        allAuthor.add(NguyenVanK);
        allAuthor.add(NguyenVanL);
        allAuthor.add(NguyenVanM);

        return allAuthor;

    }

}
