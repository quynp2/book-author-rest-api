package com.devcamp.authorbookrestapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.authorbookrestapi.model.Book;

@Service
public class BookService {

    @Autowired
    private AuthorService authorService;

    Book Book01 = new Book("Tôi thấy hoa vàng trên cỏ xanh", 150000, 5);

    Book Book02 = new Book("Yêu e từ cái nhìn đầu tiên", 20000, 7);

    Book Book03 = new Book("Con chó đốm", 150000, 8);

    Book Book04 = new Book("Ke khong nha", 2000000, 90);

    public ArrayList<Book> getAllBook() {
        ArrayList<Book> AllBook = new ArrayList<>();
        Book01.setAuthor(authorService.getAuthorOfBook1());
        Book02.setAuthor(authorService.getAuthorOfBook2());
        Book03.setAuthor(authorService.getAuthorOfBook3());
        Book04.setAuthor(authorService.getAuthorOfBook4());
        ;
        AllBook.add(Book01);
        AllBook.add(Book02);
        AllBook.add(Book03);
        AllBook.add(Book04);

        return AllBook;

    }

}
